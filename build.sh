#!/bin/bash

set -e

select_multiarchname()
{
    echo "Please select the desired architecture:"
    echo "1): i386-linux-gnu"
    echo "2): x86_64-linux-gnu"
    echo "3): armeabi-linux-android"
    echo "4): armeabi_v7a-linux-android"
    read SELECTEDOPTION
    if [ x"$SELECTEDOPTION" = x"1" ]; then
        MULTIARCHNAME=i386-linux-gnu
    elif [ x"$SELECTEDOPTION" = x"2" ]; then
        MULTIARCHNAME=x86_64-linux-gnu
    elif [ x"$SELECTEDOPTION" = x"3" ]; then
        MULTIARCHNAME=armeabi-linux-android
    elif [ x"$SELECTEDOPTION" = x"4" ]; then
        MULTIARCHNAME=armeabi_v7a-linux-android
    else
        echo "Invalid option selected!"
        select_multiarchname
    fi
}

linux_build()
{
    if [ x"$BUILDTYPE" = xdebug ]; then
        CONFIGUREOPTIONS="--enable-debug"
    else
        CONFIGUREOPTIONS="--disable-debug"
    fi

    if [[ ( -z "$OPTIMIZATION" ) || ( "$OPTIMIZATION" -eq 3 ) ]]; then
        CONFIGUREOPTIONS="$CONFIGUREOPTIONS --enable-opt"
    elif [ "$OPTIMIZATION" -eq 0 ]; then
        CONFIGUREOPTIONS="$CONFIGUREOPTIONS --disable-opt"
    else
        echo 'Optimization level '$OPTIMIZATION' is not yet implemented!'
        exit 1
    fi

    mkdir "$BUILDDIR"

    ( cd source
      ./bootstrap )

    ( OGGDIR="${PWD}/../library-ogg/${MULTIARCHNAME}"
      VORBISDIR="${PWD}/../library-vorbis/${MULTIARCHNAME}"
      export CPPFLAGS="-I${OGGDIR}/include -I${VORBISDIR}/include"
      export LDFLAGS="-L${OGGDIR}/lib -L${VORBISDIR}/lib"
      cd "$BUILDDIR"
      ../source/configure \
         --prefix="$PREFIXDIR" \
         --disable-static \
         $CONFIGUREOPTIONS
      make -j $(nproc)
      make install
      rm -rf "$PREFIXDIR"/{bin,lib/*.la} )

    # clean up afterwards
    ( cd source
      git clean -df .
      git checkout . )
}

if [ -z "$MULTIARCHNAME" ]; then
    echo "\$MULTIARCHNAME is not set!"
    select_multiarchname
fi

if [ -z "$BUILDDIR" ]; then
    BUILDDIR="build_$MULTIARCHNAME"
fi

if [ -z "$PREFIXDIR" ]; then
    PREFIXDIR="$PWD/$MULTIARCHNAME"
fi

case "$MULTIARCHNAME" in
i386-linux-gnu)
    linux_build;;
x86_64-linux-gnu)
    linux_build;;
*)
    echo "$MULTIARCHNAME is not (yet) supported by this script."
    exit 1;;
esac

install -m664 source/doc/license.txt "$PREFIXDIR"/lib/LICENSE-audiere.txt

rm -rf "$BUILDDIR"

echo "Audiere for $MULTIARCHNAME is ready."
