Website
=======
http://audiere.sourceforge.net/

License
=======
LGPL (see the file source/doc/license.txt)

Version
=======
trunk as of the 6.10.2013, revision 704

Source
======
http://sourceforge.net/p/audiere/code/HEAD/tree/trunk/audiere/

Requires
========
* Ogg
* Vorbis
